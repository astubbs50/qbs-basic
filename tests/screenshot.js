var casper, url, file, width, height, delay;

casper = require( "casper" ).create();

url = casper.cli.get( 0 );
file = casper.cli.get( 1 );
width = casper.cli.get( 2 );
height = casper.cli.get( 3 );
delay = casper.cli.get( 4 );

if ( ! delay ) {
	delay = 0;
}

casper.echo( "delay: " + delay );

//Set the viewport size
casper.options.viewportSize = { width: width, height: height };

casper.start( url, function () {
	"use strict";
	this.wait( delay, function () {
		this.capture( file );
	} );
} );

casper.then( function () {
	"use strict";
	casper.echo( "done" );
	casper.exit();
} );

casper.run();
