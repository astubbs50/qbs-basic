( function () {
	"use strict";
	var style;
	style = document.createElement( "style" );
	style.innerHTML = "canvas.qbs-screen{background-color:#000;image-rendering:optimizeSpeed;image-rendering:-moz-crisp-edges;image-rendering:-o-crisp-edges;image-rendering:-webkit-optimize-contrast;image-rendering:pixelated;image-rendering:optimize-contrast;-ms-interpolation-mode:nearest-neighbor;font-smooth:never;background-color:#000}canvas.qbs-full-screen{width:100%;height:100%;position:absolute;left:0;top:0;bottom:0;right:0;margin:auto}div.qbs-screen-container{background-color:#888;overflow-y:hidden}div.qbs-full-screen-container{background-color:#888;width:100%;height:100%;position:absolute;left:0;top:0;bottom:0;right:0;overflow-y:hidden}";
	document.head.appendChild( style );
} )();
