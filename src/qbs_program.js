/*
* File: qbs_program.js
* Description: This file is responsible for running a basic program.
*/
( function ( qbs ) {
	"use strict";

	var api,
		basic_commands,
		program,
		delay,
		auto_pause,
		qbs_canvas,
		qbs_graphics,
		qbs_document,
		stdout;

	//Create links to the canvas and graphics
	function init( _qbs_canvas, _qbs_graphics ) {
		qbs_canvas = _qbs_canvas;
		qbs_graphics = _qbs_graphics;
		qbs_document = {
			"print": function ( msg ) {
				document.body.innerHTML += msg;
			}
		};
		stdout = qbs_document;
	}

	//Start the program
	function start( code ) {
		program = basic.parse( code );

		requestAnimationFrame( run );
	}

	//Stop the program
	function stop() {
		program.line = program.code.length;
	}

	//Main loop for the program
	function run( t_start ) {
		var code_line, line, t_now;

		t_now = t_start;
		delay = false;
		while( program.line < program.code.length && ! delay && t_now < t_start + auto_pause ) {

			//Get the current line number
			line = program.line;

			//Advance to next line number
			program.line++;

			//Get the line of code to execute
			code_line = program.code[ line ];

			//Execute the command with parameters
			basic_commands[ code_line[ 0 ] ]( code_line[ 1 ] );

			//Get current time
			t_now = window.performance.now();
		}

		//Update the canvas image data
		qbs_graphics.draw();

		if( program.line < program.code.length ) {
			requestAnimationFrame( run );
		}
	}

	//The goto command
	function _goto( line_label ) {
		program.line = program.line_numbers[ line_label ];
	}

	//Print a message to the screen
	function _print( msg ) {
		stdout.print( msg );
	}

	//Print a message to the screen from memory
	function _print_m( val ) {
		stdout.print( program.memory[ val ] );
	}

	//Set the screen mode
	function _screen( screen ) {
		if( parseInt( screen ) < 0 ) {
			stdout = qbs_document;
		} else {
			stdout = qbs_graphics;
			qbs_canvas.screen( screen );
		}
	}

	//Assign a value to memory
	function _let( params ) {
		program.memory[ params[ 0 ] ] = params[ 1 ];
	}

	//Assign a value to memory from another memory location
	//Copy by value
	function _let_m( params ) {
		program.memory[ params[ 0 ] ] = program.memory[ params[ 1 ] ];
	}

	//Set the amount of time (ms) to run before pausing to release the thread.
	auto_pause = 10;

	//Setup the commands that run in basic programs
	basic_commands = {
		"goto": _goto,
		"let": _let,
		"let_m": _let_m,
		"print": _print,
		"print_m": _print_m,
		"screen": _screen
	};

	//API
	api = {
		"init": init,
		"start": start,
		"stop": stop
	};

	//Return the api to the root object
	qbs.add_module( "qbs_program", api );

} )( qbs );
