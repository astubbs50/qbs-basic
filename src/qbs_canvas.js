/*
* File: qbs_canvas.js
* Description: This file is responsible for setting up and resizing the canvas.
*/
( function ( qbs ) {
	"use strict";

	var commands,
		qbs_graphics,
		canvas,
		context,
		parent_element,
		is_fullscreen,
		pal,
		f_color;

	//Pads a string to the left with gaps or spaces
	function pad_l( str, len, gap ) {
		var i, pad;
		if( ! gap ) {
			gap = " ";
		}
		pad = "";
		str = str + "";
		for( i = str.length; i < len; i++ ) {
			pad += gap;
		}
		return pad + str;
	}

	//Check if a value is numeric
	function is_numeric( n ) {
		return ! isNaN( parseFloat( n ) ) && isFinite( n );
	}

	//Convert a number to binary string
	function dec_2_bin( dec ) {
		return ( dec >>> 0 ).toString( 2 );
	}

	//Initialize the graphics
	function init( _qbs_graphics ) {
		qbs_graphics = _qbs_graphics;
	}

	//Setup the canvas
	function setup_canvas() {
		if( canvas ) {
			if( is_fullscreen ) {
				canvas.parentElement.parentElement.removeChild( canvas.parentElement );
			} else {
				canvas.parentElement.removeChild( canvas );
			}
		}
		create_canvas();
		resize_canvas( canvas );
		window.addEventListener( "resize", resize_window );

		//Resize the window ASAP
		setTimeout( resize_window, 1 );
	}

	//Create the canvas
	function create_canvas() {
		canvas = document.createElement( "canvas" );
		canvas.className = "qbs-screen";
		context = canvas.getContext( "2d" );
		if( parent_element ) {
			is_fullscreen = false;
			parent_element.className = "qbs-screen-container";
		} else {
			is_fullscreen = true;
			parent_element = document.createElement( "div" );
			parent_element.className = "qbs-full-screen-container";
			document.body.appendChild( parent_element );
		}

		parent_element.appendChild( canvas );
	}

	//Window on resize function
	function resize_window() {
		resize_canvas( canvas );
	}

	//Resize the canvas to be max size while maintaining aspect ratio
	function resize_canvas( canvas ) {
		var ratio1, ratio2, width, height, offset;

		ratio1 = ( canvas.height / canvas.width );
		ratio2 = ( canvas.width / canvas.height );

		width = parent_element.offsetHeight * ratio2;
		height = parent_element.offsetWidth * ratio1;
		if( width > parent_element.offsetWidth ) {

			//Set the width to full width
			width = parent_element.offsetWidth;

			//Calculate the best fit height
			height = width * ratio1;

			//Calucate the remainder to center the canvas vertically
			offset = ( parent_element.offsetHeight - height ) / 2;

			//Set the margins
			canvas.style.marginLeft = "0";
			canvas.style.marginTop = Math.floor( offset ) + "px";
		} else {

			//Set the height to full height
			height = parent_element.offsetHeight;

			//Calucate the remainder to center the canvas horizontally
			offset = ( parent_element.offsetWidth - width ) / 2;

			//Set the margins
			canvas.style.marginLeft = Math.floor( offset ) + "px";
			canvas.style.marginTop = "0";
		}

		//Set the size
		canvas.style.width = Math.floor( width ) + "px";
		canvas.style.height = Math.floor( height ) + "px";
	}

	//Screen command
	function screen( screen ) {
		var resolution, width, height, screen_data, parts, print_cursor, font;

		if( typeof screen === "string" ) {
			resolution = screen.toLowerCase();
			pal = qbs_settings.qbs.palettes.pal_256;
			f_color = 15;
		} else {

			//Setup for canvas
			setup_canvas();

			if( screen < qbs_settings.qbs.screens.length ) {
				screen_data = qbs_settings.qbs.screens[ screen ];
				resolution = screen_data.size;
				pal = qbs_settings.qbs.palettes[ screen_data.pal ];
				f_color = screen_data.f_color;
				font = screen_data.font;
			} else {
				resolution = "640x480";
				pal = qbs_settings.qbs.palettes.pal_256;
				f_color = 15;
			}

		}
		if( resolution.indexOf( "x" ) > -1 ) {
			parts = resolution.split( "x" );
			width = parseInt( parts[ 0 ] );
			height = parseInt( parts[ 1 ] );

			//Calculate a font based on screen height
			if( ! font ) {
				font = Math.min( Math.max( Math.floor( height / 28 ), 6 ), 64 );
			}
		} else {
			return "Invalid screen input.";
		}
		canvas.width = width;
		canvas.height = height;
		canvas.style.width = width + "px";
		canvas.style.height = height + "px";

		print_cursor = setup_font( font );

		qbs_graphics.setup( {
			"canvas": canvas,
			"context": context,
			"pal": pal,
			"f_color": f_color,
			"print_cursor": print_cursor
		} );
	}

	//Finds the nearest font to the preset fonts based on height
	function find_nearest_font( font_size ) {
		var diff, max, found, fonts;

		fonts = {
			"6": "qbs-font-6x6",
			"8": "qbs-font-8x8",
			"14": "qbs-font-8x14",
			"16": "qbs-font-8x16"
		};

		found = false;
		max = 12;
		diff = 0;
		while( diff < max ) {
			if( fonts[ ( font_size + diff ) ] ) {
				return fonts[ ( font_size + diff ) ];
			}
			if( fonts[ ( font_size - diff ) ] ) {
				return fonts[ ( font_size - diff ) ];
			}
			diff += 1;
		}
		return false;
	}

	//Set screen parent command
	function set_screen_parent( selector ) {
		is_fullscreen = false;
		parent_element = document.getElementById( selector );
		parent_element.innerHTML = "";
	}

	//Setup the font
	function setup_font( font_data ) {
		var font_size, qbs_font, print_cursor;

		if( is_numeric( font_data ) ) {
			font_size = parseInt( font_data );
			qbs_font = find_nearest_font( font_size );
		} else {
			qbs_font = font_data;
		}

		if( qbs_settings[ "qbs-font-" + qbs_font ] ) {
			print_cursor = {};
			print_cursor.font = load_min_font( qbs_settings[ "qbs-font-" + qbs_font ] );
			print_cursor.font_size = print_cursor.font.height;
			print_cursor.print_method = "qbs_print";
			print_cursor.char_width = print_cursor.font.width;
			print_cursor.char_height = print_cursor.font.height + 1;
		} else {
			font_size = parseInt( font_data );
			return set_context_font( get_font_name( font_size ), font_size );
		}
		return print_cursor;
	}

	//Get the font name for a css font
	function get_font_name( font_size ) {
		return ( font_size ) + "px 'Lucida Console', Monaco, monospace";
	}

	//Set the font for a context font
	function set_context_font( font_name, font_size ) {
		var print_cursor;
		context.font = font_name;
		context.textBaseline = "top";
		print_cursor = {};
		print_cursor.font_size = font_size;
		print_cursor.font = font_name;
		print_cursor.print_method = "context_print";
		print_cursor.char_width = context.measureText( "M" ).width;
		print_cursor.char_height = font_size + Math.round( font_size * 0.3 );
		return print_cursor;
	}

	//Load qbs formated font
	function load_min_font( data ) {
		var i, row, x, y, font;

		font = {
			width: data.width,
			height: data.height
		};
		for( i in data ) {
			if( i === "width" || i === "height" ) {
				continue;
			}
			font[ i ] = [];
			for( y = 0; y < data[ i ].length; y++ ) {
				font[ i ].push( [] );
				row = pad_l( dec_2_bin( data[ i ][ y ] ), data.width, "0" ).split( "" );
				for( x = 0; x < row.length; x++ ) {
					font[ i ][ y ].push( parseInt( row[ x ] ) );
				}
			}
		}
		return font;
	}

	//Set to fullscreen by default
	is_fullscreen = true;

	commands = {
		"set_screen_parent": set_screen_parent,
		"screen": screen,
		"init": init
	};

	//Return the api to the root object
	qbs.add_module( "qbs_canvas", commands );

} )( qbs );
