/*
* File: qbs_graphics.js
* Description: This file is responsible for all the graphics routines.
*/

( function ( root ) {
	"use strict";

	var commands,
		canvas,
		context,
		canvas_data,
		canvas_buffer,
		context_buffer,
		dirty,
		pal,
		f_color,
		image_data,
		print_cursor,
		print_method;

	function hexToRgb( hex ) {
		var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec( hex );
		return result ? {
			r: parseInt( result[ 1 ], 16 ),
			g: parseInt( result[ 2 ], 16 ),
			b: parseInt( result[ 3 ], 16 ),
			a: 255,
			s: hex
		} : null;
	}

	function cToHex( c ) {
		var hex = Number( c ).toString( 16 );
		if ( hex.length < 2 ) {
			hex = "0" + hex;
		}
		return hex.toUpperCase();
	}

	function rgbToHex( r, g, b ) {
		return "#" + cToHex( r ) + cToHex( g ) + cToHex( b );
	}

	function compare_colors( color_1, color_2 ) {
		return color_1.r === color_2.r && color_1.g === color_2.g && color_1.b === color_2.b;
	}

	function create_canvas_buffer( canvas ) {
		canvas_buffer = document.createElement( "canvas" );
		canvas_buffer.width = canvas.width;
		canvas_buffer.height = canvas.height;
		context_buffer = canvas_buffer.getContext( "2d" );
	}

	//Setup the graphics
	function setup( settings ) {
		var i;
		dirty = false;

		//Get the canvas
		canvas = settings.canvas;

		//Get the context
		context = settings.context;

		//Get the image data
		image_data = context.getImageData( 0, 0, canvas.width, canvas.height );

		//Create the graphics buffer
		create_canvas_buffer( canvas );

		//Get the canvas data
		canvas_data = {
			width: canvas.width,
			height: canvas.height
		};

		//Get the palette
		pal = settings.pal.slice();

		//Get the fore color
		f_color = settings.f_color;

		//Get the print cursor
		print_cursor = settings.print_cursor;

		//Set the default print cursor to 0 x 0
		print_cursor.x = 0;
		print_cursor.y = 0;

		//Set the print method
		if( print_cursor.print_method === "qbs_print" ) {
			print_cursor.print_method = qbs_print;
		} else {
			print_cursor.print_method = context_print;
		}

		//Convert pal colors to rgbas
		for( i = 1; i < pal.length; i++ ) {
			pal[ i ] = hexToRgb( pal[ i ] );
		}

		//Set color 0 to have no alpha
		pal[ 0 ] = { r: 0, g: 0, b: 0, a: 0, s: "#000000" };

	}

	//Sets the cursor position
	function set_cursor( gx, gy ) {
		if( print_method === context_print ) {
			print_cursor.x = gx * print_cursor.char_width;
			print_cursor.y = gy * print_cursor.char_height - 2;
		} else {
			print_cursor.x = gx * print_cursor.char_width;
			print_cursor.y = gy * print_cursor.char_height;
		}
	}

	//Puts an array of integers onto the screen
	function put( data, x, y, _pal ) {
		var data_x, data_y, start_x, start_y, width, height, i, c, i2;

		//Exit if no data
		if( ! data || data.length < 1 ) {
			return;
		}

		//Clamp x & y
		if( x < 0 ) {
			start_x = x * -1;
		} else {
			start_x = 0;
		}
		if( y < 0 ) {
			start_y = y * -1;
		} else {
			start_y = 0;
		}

		//Calc width & height
		width = data[ 0 ].length - start_x;
		height = data.length - start_y;

		//Clamp width & height
		if( x + start_x + width >= canvas_data.width ) {
			width = canvas_data.width - x + start_x;
		}
		if( y + start_y + height >= canvas_data.height ) {
			height = canvas_data.height - y + start_y;
		}

		//Exit if there is no data that fits the screen
		if( width <= 0 || height <= 0 ) {
			return;
		}

		//Loop through the data
		i = 0;
		for( data_y = start_y; data_y < start_y + height; data_y++ ) {
			for( data_x = start_x; data_x < start_x + width; data_x++ ) {

				//Get the color
				c = data[ data_y ][ data_x ];

				if( ! isNaN( Number( c ) ) ) {
					if( c >= _pal.length ) {
						c = hexToRgb( "#000000" );
					}
					c = _pal[ c ];
				} else {
					c = hexToRgb( c );
				}

				//Calculate the index of the image data
				i = ( ( canvas_data.width * ( y + data_y ) ) + ( x + data_x ) ) * 4;

				//Put the color in the image data
				if( c.a > 0 ) {
					image_data.data[ i ] = c.r;
					image_data.data[ i + 1 ] = c.g;
					image_data.data[ i + 2 ] = c.b;
					image_data.data[ i + 3 ] = c.a;
				}
			}
		}

		//Put the image data back on the canvas
		dirty = true;
	}

	function print( msg, in_line ) {
		var width, overlap, on_screen, on_screen_pct, msg_split, msg_1, msg_2;

		msg = "" + msg;

		//Adjust if the text is too wide for the screen
		width = print_cursor.char_width * msg.length;
		if( width + print_cursor.x > canvas_data.width && ! in_line && msg.length > 1 ) {
			overlap = ( width + print_cursor.x ) - canvas_data.width;
			on_screen = width - overlap;
			on_screen_pct = on_screen / width;
			msg_split = Math.floor( msg.length * on_screen_pct );
			msg_1 = msg.substring( 0, msg_split );
			msg_2 = msg.substring( msg_split, msg.length );
			print( msg_1, in_line );
			print( msg_2, in_line );
			return;
		}

		//Adjust if the text is too tall for the screen
		if( print_cursor.y + print_cursor.char_height > canvas_data.height ) {

			//Draw the canvas
			draw();

			//Draw the image on the buffer
			context_buffer.clearRect( 0, 0, canvas_data.width, canvas_data.height );
			context_buffer.drawImage( canvas, 0, 0 );

			//Clear the current context and draw the buffer image
			context.clearRect( 0, 0, canvas_data.width, canvas_data.height );
			context.drawImage( canvas_buffer, 0, -print_cursor.char_height );
			refresh_image_data();

			//Backup the print_cursor
			print_cursor.y -= print_cursor.char_height;
		}

		print_cursor.print_method( msg, print_cursor.x, print_cursor.y );

		//If it's not in_line print the advance to next line
		if( ! in_line ) {
			print_cursor.y += print_cursor.char_height;
			print_cursor.x = 0;
		}
	}

	//Print to the screen by using qbs_fonts
	function qbs_print( msg, x, y ) {
		var _pal, i;

		//Set the palette to be 2 colors with the background color and the foreground color
		_pal = [
			{ r: 0, g: 0, b: 0, a: 0, s: "#000000" },
			pal[ f_color ]
		];

		//Loop through each character in the message and put it on the screen
		for( i = 0; i < msg.length; i++ ) {
			put( print_cursor.font[ msg[ i ] ], x + print_cursor.font.width * i, y, _pal );
		}
	}

	//Print to the screen using the context fill text method
	function context_print( msg, x, y ) {
		context.fillStyle = pal[ f_color ].s;
		context.fillText( msg, x, y + 2 );
		refresh_image_data();
	}

	//Refresh the image data
	function refresh_image_data() {
		image_data = context.getImageData( 0, 0, canvas_data.width, canvas_data.height );
	}

	//Draw the image data onto the canvas
	function draw() {
		if( dirty ) {
			context.putImageData( image_data, 0, 0 );
			dirty = false;
		}
	}

	//Commands
	commands = {
		"setup": setup,
		"print": print,
		"draw": draw
	};

	//Return the api to the root object
	qbs.add_module( "qbs_graphics", commands );

} )( qbs );
