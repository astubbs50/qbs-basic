/*
* File: qbs.js
* Description: This file is responsible for exposing the public API to the user and
* compiles and runs basic.
*/
window.qbs = ( function () {
	"use strict";

	var api,
		qbs_canvas,
		qbs_graphics,
		qbs_program,
		start_functions,
		initialized,
		modules_count;

	//Compiles and runs a basic program
	function run( code ) {
		if( initialized ) {
			qbs_program.start( code );
		} else {
			start_functions.push( function () {
				run( code );
			} );
		}
	}

	//Copies properties from one object to another
	function copy_properties( obj1, obj2 ) {
		var prop;
		for( prop in obj2 ) {
			if( obj2.hasOwnProperty( prop ) ) {
				obj1[ prop ] = obj2[ prop ];
			}
		}
	}

	//Adds the a module to this scope
	function add_module( name, api ) {
		switch( name ) {
			case "qbs_graphics":
				qbs_graphics = api;
				break;
			case "qbs_canvas":
				qbs_canvas = api;
				break;
			case "qbs_program":
				qbs_program = api;
				break;
		}

		//When all modules are loaded then initialize qbs
		modules_count += 1;
		if( modules_count === 3 ) {
			initialize();
		}
	}

	//Setup the basic commands
	function initialize() {
		var commands, i;

		//Add some additional commands that can only be set
		//after all modules are loaded.
		commands = {
			"set_screen_parent": qbs_canvas.set_screen_parent,
			"screen": qbs_canvas.screen,
			"print": qbs_graphics.print,
			"stop": qbs_program.stop
		};

		//Copy commands to api
		copy_properties( api, commands );

		//Init the program runner
		qbs_program.init( qbs_canvas, qbs_graphics );

		//Init the canvas script
		qbs_canvas.init( qbs_graphics );

		//Set flag as initialized
		initialized = true;

		//Call all start functions
		for( i = 0; i < start_functions.length; i++ ) {
			start_functions[ i ]();
		}
	}

	//Set the state to not initialized
	initialized = false;

	//No modules are loaded yet
	modules_count = 0;

	//A list of function to run hen document is ready
	start_functions = [];

	document.addEventListener( "ready", initialize );

	//Setup commands that will run only in the qbs api
	api = {
		"add_module": add_module,
		"run": run,
		"stop": stop
	};

	return api;

} )();
