{
	function set_var( name, val, ty ) {
    	program.memory[ name ] = t[ ty ]().v;
        checker[ name ] = ty;
    }

	var t = {
    	"INT": function ( val, mem ) {
        	if( val === undefined ) {
            	val = 0;
            }
            if( mem === undefined ) {
            	mem = null;
            }
        	return {
            	"t": "INT",
                "v": val,
                "m": mem
            };
        },
        "STR": function ( val, mem ) {
        	if( val === undefined ) {
            	val = "";
            }
            if( mem === undefined ) {
            	mem = null;
            }
        	return {
            	"t": "STR",
                "v": val,
                "m": mem
            };
        }
    }

	var program = {
		"code": [],
		"line": 0,
		"line_numbers": {},
		"memory": {}
	};
    var current_line = 0;
    var checker = {};
}

start
  = lines:lines {
    program.code = lines;
  	return program;
  }

lines
  = lines:line* {
  	var code = [];
    //Remove blank lines from code
    for( var i = 0; i < lines.length; i++ ) {
        if( lines[ i ] ) {
        	code.push( lines[ i ] );
        }
    }
  	return code;
  }

line
  = _ line:line_number* _ statement:statement _ {
  	if( statement ) {
    	current_line++;
    }
    return statement;
  }

expression
  = term: term {
  	return term;
  }
  / name: variable {
  	if( checker[ name ] === "INT" ) {
    	return t.INT( null, name );
    } else if ( checker[ name ] === "STR" ) {
    	return t.STR( null, name );
    }
  	return vari;
  }

term
  = int:inline_integer {
  	return t.INT( int );
  }
  / str:inline_string {
  	return t.STR( str, null );
  }

inline_integer
  = digits:([-]?[0-9]+) {
  	var s_digits = digits.map( function ( d ) {
      if( Array.isArray( d ) ) {
        return d.join( "" );
      } else {
        return d;
      }
    } ).join( "" );
  	return parseInt( s_digits ); 
  }

inline_string "string"
  = quotation_mark chars:char* quotation_mark { return chars.join(""); }

char =
	[^\x22]

quotation_mark
  = '"'

unescaped
  = [^\0-\x1F\x22\x5C]

DIGIT  = [0-9]
HEXDIG = [0-9a-f]i

line_number
  = digits:[0-9]+ {
    if( program.line_numbers[ digits.join("") ] != null ) {
      return error( "Duplicate label - " + digits.join("") + "." );
    } else {
  		program.line_numbers[ digits.join("") ] = current_line;
    }
    return "#";
  }

_ "whitespace"
  = [ \t\n\r]* {
  	return "_";
  }

variable "variable"
  = name:([_a-zA-Z]+[_a-zA-Z0-9]*) {
  	return name.toString().split(",").join("");
  }

type "type"
  = "INTEGER"i {
  	return t.INT( 0, null );
  }
  / "STRING"i {
  	return t.STR( "", null );
  }

statement
  = PRINT
  / GOTO
  / SCREEN
  / DIM
  / LET
  / _SET_SCREEN_PARENT

PRINT "PRINT"
  = "PRINT"i _ exp:expression {
  	if( exp.m !== null ) {
    	return [ "print_m", exp.m ];
    } else {
		return [ "print", exp.v ];
	}
  }

GOTO "GOTO"
  = "GOTO"i _ num:inline_integer {
  	return [ "goto", num ];
  }

SCREEN "SCREEN"
  = "SCREEN"i _ exp:expression {
  	if( exp.m !== null ) {
    	return [ "screen_m", exp.m ];
    } else {
		return [ "screen", exp.v ];
	}
  }

DIM "DIM"
  = "DIM"i _ name:variable _ "AS"i _ ty:type {
 	if( program.memory[ name ] == null ) {
        set_var( name, ty.v, ty.t );
    	return false;
    } else {
    	return error( name + " already in use." );
    }
  }

LET "LET"
  = "LET"i* _ name:variable _ "=" _ exp:expression {
  	if( program.memory[ name ] == null ) {
    	set_var( name, exp.v, exp.t );
    }
  	if( checker[ name ] === exp.t ) {
    	if( exp.m !== null ) {
            return [ "let_m", [ name, exp.m ] ];
        } else {
            return [ "let", [ name, exp.v ] ];
        }
    } else {
    	return error( "Invalid type conversion." );
    }
  }

_SET_SCREEN_PARENT "_SET_SCREEN_PARENT"
  = "_SET_SCREEN_PARENT"i _ exp:expression {
    return [ "_set_screen_parent", exp ];
  }
