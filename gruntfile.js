module.exports = function ( grunt ) {
	"use strict";

	// Project configuration.
	grunt.initConfig( {
		pkg: grunt.file.readJSON( "package.json" ),
		cssmin: {
			target: {
				files: {
					"src/qbs.min.css": [ "src/qbs.css" ]
				}
			}
		},
		uglify: {
			options: {
				banner: "/*! <%= pkg.name %> <%= grunt.template.today('yyyy-mm-dd') %> */\n",
				verbose: true,
				sourceMap: true
			},
			build: {
				src: [
					"src/qbs.js",
					"src/qbs_styles.js",
					"src/qbs_program.js",
					"src/qbs_canvas.js",
					"src/qbs_graphics.js",
					"src/qbs_settings.js",
					"src/basic.js"
				],
				dest: "build/<%= pkg.name %>.min.js"
			}
		},
		json: {
			main: {
				options: {
					namespace: "qbs_settings",
					includePath: true,
					processName: function ( filename ) {
						var start, end, name;

						start = filename.lastIndexOf( "/" ) + 1;
						end = filename.lastIndexOf( ".json" );
						name = filename.substring( start, end );
						return name;
					}
				},
				src: [ "src/settings/*.json" ],
				dest: "src/qbs_settings.js"
			}
		}
	} );

	grunt.loadNpmTasks( "grunt-json" );
	grunt.loadNpmTasks( "grunt-contrib-uglify" );
	grunt.loadNpmTasks( "grunt-contrib-cssmin" );

	//Build basic parser task
	grunt.registerTask( "build-basic", function () {
		var cmd, cmd_str, done;

		//Log message
		console.log( "Building basic.js." );

		//Get the done signal
		done = this.async();

		//Import node cmd
		cmd = require( "node-cmd" );

		//Set the command to run pegjs to create the basic parser
		cmd_str = "pegjs --format \"globals\" --export-var \"basic\" src/basic.pegjs";

		//run the command
		cmd.get( cmd_str, function ( err, data, stderr ) {
			if( err ) {
				console.log( err );
			}
			if( data ) {
				console.log( data );
			}
			if( stderr ) {
				console.log( stderr );
			}
			done( "Basic Build Completed" );
		} );
	} );

	//Create stylesheet task
	grunt.registerTask( "create-styles", function () {
		var done, fs, code;

		//Read the code from file
		function read_code() {
			fs.readFile( "src/qbs_styles_code.js", function ( err, data ) {
				if( err ) {
					console.log( err );
				} else {
					code = data.toString();
					read_styles();
				}
			} );
		}

		//Read the styles from file
		function read_styles() {
			fs.readFile( "src/qbs.min.css", function ( err, data ) {
			if( err ) {
					console.log( err );
				} else {
					code = code.replace( "[CSS]", data.toString() );
					fs.writeFile( "src/qbs_styles.js", code, function () {
						console.log( "QBS Styles Completed" );
						done( "QBS Styles Completed" );
					} );
				}
			} );
		}

		//Get the file system handler
		fs = require( "fs" );

		//Log message
		console.log( "Building qbs_styles.js." );

		//Get the done signal
		done = this.async();

		read_code();
	} );

	grunt.registerTask( "default", [
		"build-basic", "cssmin", "create-styles", "json", "uglify"
	] );

};
